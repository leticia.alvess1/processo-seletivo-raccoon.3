var cart_products = [{
    'product_id': 'IN2350',
    'product_name': 'Teclado Mecânico Rozar K600',
    'product_price': '650.00',
    'product_quantity': '1',
    'product_url': 'http://www.foo.com/p/IN2350',
    'product_category': 'peripherals',
    'product_brand': 'Rozar'
}, {
    'product_id': 'IN4566',
    'product_name': 'Monitor Gamer LED GL UltraWide 25"',
    'product_price': '1399.99',
    'product_quantity': '1',
    'product_url': 'http://www.foo.com/p/IN4566',
    'product_category': 'monitor',
    'product_brand': 'GL'
}, {
    'product_id': 'ES7112',
    'product_name': 'Pacote de Post-it Fenix 50 unidades',
    'product_price': '14.95',
    'product_quantity': '5',
    'product_url': 'http://www.foo.com/p/ES7112',
    'product_category': 'office',
    'product_brand': 'Fenix'
}, {
    'product_id': 'BR8810',
    'product_name': 'Miniatura Yoshi Haras',
    'product_price': '6.50',
    'product_quantity': '2',
    'product_url': 'http://www.foo.com/p/BR8810',
    'product_category': 'others',
    'product_brand': 'Haras'
}];

var item_price = cart_products.map (product => product.product_price)
item_price = item_price.map(item => parseFloat(item, 10))

var totalPrice = item_price.reduce((acum, num) => acum + num) 
totalPrice = totalPrice.toFixed(2)

if (totalPrice > 300) {
    var freeShipping = true
} else {
    var freeShipping = false
}

var cartPricesInfo = {totalPrice, freeShipping}

console.log({cartPricesInfo})
console.log(`totalPrice = ${totalPrice};\nfreeShiping = ${freeShipping};`)

